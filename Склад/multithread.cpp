#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <string>
#include <utility>
#include <vector>
#include <array>
#include <iterator>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <stdexcept>
#include <condition_variable>
#include <pthread.h>
#include <semaphore.h>
#include "progtest_solver.h"
#include "sample_tester.h"

using namespace std;
#endif /* __PROGTEST__ */

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
//All information about pack
struct PackInfo{
    AProblemPack pack;
    int packageNumber;
    int companyId;
    //Is false for all packs except last. Last pack is not returned to company. Last pack will signalize that company is dead (no more packs).
    bool isLast;
    //How many problems are solved
    size_t solved;
    //How many problems are in pack
    size_t total;
    PackInfo(AProblemPack pack1, int num, int id, bool isLast1 = false )
            : pack(move(pack1)), packageNumber(num), companyId(id), isLast(isLast1), solved(0){
        if(pack)
            total = pack->m_Problems.size();
        else
            total = 0;
    }
};

//Each company has own buffer with mutex, condition_variable and vector of packs.
struct CompanyBuffer{
    mutex mtx;
    vector<shared_ptr<PackInfo>> vec;
    condition_variable cond;

    //Update information in pack. If pack is not in buffer, add it and create empty packs between last exists pack and new pack.
    void updatePackInfo(shared_ptr<PackInfo> &newPack){
        mtx.lock();
        if(vec.size() <= (size_t)newPack->packageNumber)
            vec.resize(newPack->packageNumber + 1 , make_shared<PackInfo>(nullptr, -1 , 0));
        vec[newPack->packageNumber] = newPack;
        mtx.unlock();
        //In this program this function is called only when pack is not solved. So we don't need to notify.
        //cond.notify_one();
    }

};

//class solver is solving problems and save information about solved problems.
struct Solver{
    AProgtestSolver solver;
    bool isSolved;
    // pair<idCompany, idPack>. Vector with problems which are added, but not solved.
    vector<pair<int, int>> readyToSolve;
    // key = pair<idCompany, idPack>; val = how many problems was solved
    map<pair<int, int>, int> readyToSolveResult;

    Solver() : solver(createProgtestSolver()), isSolved(false){}
    //Solve all problems in pack. Can be called only once.
    bool solve(){
        if(isSolved)
            return false;
        solver->solve();
        isSolved = true;

        //Save information about solved problems
        for(auto &problem : readyToSolve)
        {
            readyToSolveResult[problem] += 1;
        }
        return true;
    }
    bool addProblem(AProblem &problem){
        return solver->addProblem(problem);
    };

    bool hasFreeCapacity(){
        return solver->hasFreeCapacity();
    };
    void addToReady( pair<int, int> problem){
        readyToSolve.push_back(problem);
    };

};

class COptimizer {
public:
    static bool usingProgtestSolver(void) {
        return true;
    }

    static void checkAlgorithm(AProblem problem) {
        // dummy implementation if usingProgtestSolver() returns true
    }

    void start(int threadCount){
        //For each company create own buffer
        for (int i = 0; (size_t)i < companies.size(); i++){
            companyBuffers.push_back(make_shared<CompanyBuffer>());
        }

        //For each company create own thread for adding pack to q_Problems and for returning pack to company.
        for (int i = 0; (size_t)i < companies.size(); i++){
            committers.emplace_back(&COptimizer::addPack, this, i);
            committers.emplace_back(&COptimizer::returnPack, this, i);
        }

        //Create threads(workers) for solving problems
        for (int i = 0; i < threadCount; i++){
            workers.emplace_back(&COptimizer::worker, this, threadCount);
        }
    }

    //Wait for all threads to finish
    void stop(void){
        for (int i = 0; (size_t)i < committers.size(); i++){
            committers[i].join();
        }

        for (int i = 0; (size_t)i < workers.size(); i++){
            workers[i].join();
        }
    }

    //Create new company
    void addCompany(const ACompany& company){
        companies.push_back(company);
    }

    //Check packs from all companies. Given pack push to queue q_Problems for solving.
    void addPack(int idCompany) {
        //Current pack number
        int idPack = 0;
        auto pack = companies[idCompany]->waitForPack();
        //If pack == nullptr, company never send pack. So we don't need to wait for pack
        while (pack)
        {
            unique_lock<mutex> lck(mtx_Problems);
            q_Problems.push(make_shared<PackInfo>(pack, idPack++, idCompany));
            lck.unlock();
            cv_Problems.notify_all();
            pack = companies[idCompany]->waitForPack();
        }

        unique_lock<mutex> lck(mtx_Problems);
        ++deadCompanies;
        //Send ended pack to queue. This pack will signalize that company is dead (no more packs).
        q_Problems.push(make_shared<PackInfo>(AProblemPack(), idPack, idCompany, true));
        cv_Problems.notify_all();
    }

    //Return solved packs. All packs are returned in order.
    void returnPack(int idCompany){
        //Current pack number which we are waiting for.
        int packNumber = 0;
        auto buffer = companyBuffers[idCompany];
        while (true)
        {
            unique_lock<mutex> lck(buffer->mtx);
            buffer->cond.wait(lck, [&]()
            {return buffer->vec.size() > (size_t)packNumber // If packNumber is exists in buffer
                    && packNumber == buffer->vec[packNumber]->packageNumber // If packNumber is correct
                    && buffer->vec[packNumber]->solved == buffer->vec[packNumber]->total;}); // If pack is solved
            auto pack = buffer->vec[packNumber];
            lck.unlock();
            //Given last pack. End thread.
            if (pack->isLast)
            {
                break;
            }
            //Send solved pack to company
            companies[idCompany]->solvedPack(pack->pack);
            //Waiting for next pack
            ++packNumber;
        }
    }

    //Call worker. Worker solve problems from queue q_Problems.
    shared_ptr<PackInfo> popPack(){
        while(true)
        {
            unique_lock<mutex> lck(mtx_Problems);
            cv_Problems.wait(lck, [&](){return !q_Problems.empty() || deadCompanies == companies.size();});

            //If all companies are dead and all problems are solved, return nullptr.
            if(q_Problems.empty() && deadCompanies == companies.size())
            {
                return nullptr;
            }

            auto pack = q_Problems.front();
            q_Problems.pop();
            //If pack is last, we need to signalize that all packs from company was solved.
            if(pack->isLast)
            {
                companyBuffers[pack->companyId]->updatePackInfo(pack);
                companyBuffers[pack->companyId]->cond.notify_one();
            }
            else
                return pack;
        }
    }

    //Update information about solved problems in pack.
    void addToReturn(shared_ptr<Solver> &tmp_solver)
    {
        for(auto &item : tmp_solver->readyToSolveResult)
        {
            companyBuffers[item.first.first]->mtx.lock();
            companyBuffers[item.first.first]->
                vec[item.first.second]->solved += item.second;
            companyBuffers[item.first.first]->mtx.unlock();
            companyBuffers[item.first.first]->cond.notify_one();
        }
        tmp_solver = nullptr;
    }

    void worker(int threadCount){
        //Local solver(only this thread can use it)
        //All workers adding problems to solver.
        // When solver is full, one worker solve problems and other workers add problems to new solver.
        shared_ptr<Solver> tmp_solver = nullptr;
        while(true)
        {
            auto currentPack = popPack();
            //All packs were solved.
            if(!currentPack)
            {
                if(++deadWorkers < (size_t)threadCount)
                    return;
                //Last worker solving remaining problems.
                solver->solve();
                addToReturn(solver);

                break;
            }
            size_t packSize = currentPack->pack->m_Problems.size();
            //Add empty pack to buffer. This pack will be updated when problems from pack will be solved.
            companyBuffers[currentPack->companyId]->updatePackInfo(currentPack);

            //Add problems from pack to solver.
            for( size_t i = 0; i < packSize; ++i)
            {
                unique_lock<mutex> lck_solver(mtx_Solver);
                //If solver is full, one worker solving problems.
                if(!solver->addProblem(currentPack->pack->m_Problems[i]))
                {
                    tmp_solver = solver;
                    solver = make_shared<Solver>();
                    solver->addProblem(currentPack->pack->m_Problems[i]);
                    solver->addToReady({currentPack->companyId, currentPack->packageNumber});
                    lck_solver.unlock();
                    tmp_solver->solve();
                    addToReturn(tmp_solver);
                }
                else
                    solver->addToReady({currentPack->companyId, currentPack->packageNumber});

            }

            unique_lock<mutex> lck_solver(mtx_Solver);
            //If solver is full and all problems from pack were added.
            if(!solver->hasFreeCapacity())
            {
                tmp_solver = solver;
                solver = make_shared<Solver>();
                lck_solver.unlock();
                tmp_solver->solve();
                addToReturn(tmp_solver);
            }
            else
                lck_solver.unlock();

        }
    }

    COptimizer()
            : solver(make_shared<Solver>()), deadCompanies(0), deadWorkers(0) {}

private:
    //Vector with companies
    vector<ACompany> companies;
    //Vector with workers call in start(threadCount)
    vector<thread> workers;
    //Vector for communication between workers and companies
    vector<thread> committers;
    //Buffer for each company
    vector<shared_ptr<CompanyBuffer>> companyBuffers;

    //Queue with packs from all companies
    queue<shared_ptr<PackInfo>> q_Problems;
    mutex mtx_Problems;
    condition_variable cv_Problems;

    //Solver. One for all workers. Solving problems from packs
    shared_ptr<Solver> solver;
    mutex mtx_Solver;

    //Number of dead companies
    size_t deadCompanies;
    //Number of dead workers
    atomic_size_t deadWorkers;

};

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__

int main(void) {
    COptimizer optimizer;
    ACompanyTest company = std::make_shared<CCompanyTest>();
    ACompanyTest company2 = std::make_shared<CCompanyTest>();
    optimizer.addCompany(company);
    optimizer.addCompany(company2);
    optimizer.start(10);
    optimizer.stop();
//    cout << "End" << endl;
    if (!company->allProcessed())
        throw std::logic_error("(some) problems were not correctly processsed");
    return 0;
}

#endif /* __PROGTEST__ */
